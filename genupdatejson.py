#!/usr/bin/python

# Copyright (C) 2016, David "Davee" Morgan

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
import os, os.path, sys, json, subprocess

version = sys.argv[1]
url = 'http://infinity.lolhax.org/updates/' + version + '.MFC'

with open('core/version_num', 'r') as myfile:
    core_version = myfile.read().rstrip();
    myfile.close();
    print("-> CORE VERSION R" + core_version);
    
with open('subset/version_num', 'r') as myfile:
    subset_version = myfile.read().rstrip();
    myfile.close();
    print("-> 6.61 VERSION R" + subset_version);
    
jsonVersion = json.dumps({'version':version, 'core':int(core_version), 'subset':int(subset_version), 'url':url }, separators=(',',':'));

with open(sys.argv[2], 'w') as myfile:
    myfile.write(jsonVersion)
    myfile.close()
