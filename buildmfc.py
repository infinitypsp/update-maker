#!/usr/bin/python

# Copyright (C) 2016, David "Davee" Morgan

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import os, os.path, sys, json, subprocess
from struct import *

class MfcContainer:
    def __init__(self):
        self.header = dict(magic=0x43464D00, nsections=0, sections=[0, 0, 0, 0, 0, 0, 0, 0]);
        self.section = dict(name = "", size = 0, nfiles = 0);
        self.entry = dict(name = "", size = 0, model = 0, signcheck = 0, loadmod = 0, dir = 0);
        self.inited = False;
        self.sectionInited = False;
        
    def init(self, file):
        self.fd = open(file, 'wb');
        self.fd.write(pack('<II8I', 0x43464D00, self.header['nsections'], *self.header['sections']));
        self.inited = True;
        self.sectionInited = False;
        
    def end(self):
        if self.inited == False or self.sectionInited == True:
            raise Exception('Could not end container, not inited or section not ended.');
        
        self.fd.seek(0);
        self.fd.write(pack('<II8I', 0x43464D00, self.header['nsections'], *self.header['sections']));
        self.fd.close();
        self.inited = False;
        
    def initSection(self, name):
        if self.inited == False or self.sectionInited == True:
            raise Exception('Could not init section. Container not inited or section already inited.');
            
        if self.header['nsections'] >= len(self.header['sections']):
            raise Exception('Insufficient sections remaining.');
        if len(name) >= 32:
            raise Exception('Section name is too long. Max = 32');
            
        self.section['name'] = name;
        self.section['size'] = 0;
        self.section['nfiles'] = 0;
        
        self.header['sections'][self.header['nsections']] = self.fd.tell();
        self.fd.write(pack('<32sII', self.section['name'], self.section['size'], self.section['nfiles']));
        self.sectionInited = True;
        
    def endSection(self):
        if self.inited == False or self.sectionInited == False:
            raise Exception('Could not end section. Container not inited or section not inited.');
        
        self.section['size'] = self.fd.tell()-self.header['sections'][self.header['nsections']]-calcsize('<32sII');
        self.fd.seek(self.header['sections'][self.header['nsections']]);
        
        self.fd.write(pack('<32sII', self.section['name'], self.section['size'], self.section['nfiles']));
        
        self.fd.seek(0, 2);
        
        self.header['nsections'] += 1;
        self.sectionInited = False;
        
    def addDirectory(self, name, model):
        if self.inited == False or self.sectionInited == False:
            raise Exception('Could not add directory. Container not inited or section not inited.');
        if len(name) >= 128:
            raise Exception('Directory name is too long. Max = 128');
            
        self.entry['name'] = name;
        self.entry['size'] = 0;
        self.entry['model'] = model;
        self.entry['signcheck'] = 0;
        self.entry['loadmod'] = 0;
        self.entry['dir'] = 1;
        
        self.fd.write(pack('<128sIIIII', self.entry['name'], self.entry['size'], self.entry['model'], self.entry['signcheck'], self.entry['loadmod'], self.entry['dir']));
        
        self.section['nfiles']+=1;
        
    def addFile(self, name, data, model, signcheck):
        if self.inited == False or self.sectionInited == False:
            raise Exception('Could not add file. Container not inited or section not inited.');
        if len(name) >= 128:
            raise Exception('File name is too long. Max = 128');
            
        self.entry['name'] = name;
        self.entry['size'] = len(data);
        self.entry['model'] = model;
        self.entry['signcheck'] = signcheck;
        self.entry['loadmod'] = 0;
        self.entry['dir'] = 0;
        
        self.fd.write(pack('<128sIIIII', self.entry['name'], self.entry['size'], self.entry['model'], self.entry['signcheck'], self.entry['loadmod'], self.entry['dir']));
        self.fd.write(data);
        
        self.section['nfiles']+=1;
        
print("Building update file: UPDATE.MFC");

container = MfcContainer();
container.init("UPDATE.MFC");
directories = set();

container.initSection("661");
print("Packing 6.61 flash0... ");

for directory, _, files in os.walk("661/flash0"):
    for file in files:
        fixedDirectory = "flash0:" + directory.replace('\\', '/')[10:];
        fixedFilename = fixedDirectory + '/' + file;
        directories.add(fixedDirectory);
        
        with open(directory + '/' + file, 'r') as myfile:
            data = myfile.read();
            myfile.close();
            container.addFile(fixedFilename, data, 0xFFFFFFFF, 0);
            print("-> " + fixedFilename);
        continue

container.endSection();

container.initSection("CORE");
print("Packing core flash0... ");

for directory, _, files in os.walk("core/flash0"):
    for file in files:
        fixedDirectory = "flash0:" + directory.replace('\\', '/')[11:];
        fixedFilename = fixedDirectory + '/' + file;
        directories.add(fixedDirectory);
        
        with open(directory + '/' + file, 'rb') as myfile:
            data = myfile.read();
            myfile.close();
            container.addFile(fixedFilename, data, 0xFFFFFFFF, 0);
            print("-> " + fixedFilename);
        continue

container.endSection();

container.initSection("UTILITY");
print("Packing utility modules... ");

for directory, _, files in os.walk("utility"):
    for file in files:
        with open(directory + '/' + file, 'rb') as myfile:
            data = myfile.read();
            myfile.close();
            container.addFile(file, data, 0xFFFFFFFF, 0);
            print("-> " + file);
        continue

container.endSection();

container.initSection("BOOTLOADER");
print("Packing bootloader... ");
with open('core/bootloader/bootloader.prx', 'rb') as myfile:
    data = myfile.read();
    myfile.close();
    container.addFile('bootloader.prx', data, 0xFFFFFFFF, 0);
    print("-> bootloader.prx");
    
with open('core/bootloader/bootloader_payload.bin', 'rb') as myfile:
    data = myfile.read();
    myfile.close();
    container.addFile('bootloader_payload.bin', data, 0xFFFFFFFF, 0);
    print("-> bootloader_payload.bin");

container.endSection();

container.initSection("UPDATER_VERSION");
print("Packing updater info... ");

with open('core/version_num', 'r') as myfile:
    core_version = myfile.read().rstrip();
    myfile.close();
    print("-> CORE VERSION R" + core_version);
    
with open('661/version_num', 'r') as myfile:
    subset_version = myfile.read().rstrip();
    myfile.close();
    print("-> 6.61 VERSION R" + subset_version);
    
jsonVersion = json.dumps({'core':int(core_version), 'subset':int(subset_version)}, separators=(',',':'));
container.addFile("version", jsonVersion, 0xFFFFFFFF, 0);
print('-> Generated JSON: ' + jsonVersion);

container.endSection();
container.initSection("DIRS");
for directory in directories:
    container.addDirectory(directory, 0xFFFFFFFF);
container.endSection();
container.end();
